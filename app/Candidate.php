<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//eloquent זה הרכיב שמממש את המודל

class Candidate extends Model
{
    protected $fillable = ['name','email'];//השדות שאנחנו מרשים לעדכן במס אסיימנט
//אם לא משתמשים במוסכמה של לארבל נציין במפורש איזה שדה אנו רוצים
    public function owner(){ //הגדרת הקשר בין המשתמש למועמד
        return $this->belongsTo('App\User','user_id'); //כל מועמד שייך למשתמש אחד
    }

    public function status()
    {
        return $this->belongsTo('App\Status'); //לכל מועמד יש סטטוס אחד
    }     

}
