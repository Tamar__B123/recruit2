<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidates()
    {
        return $this->hasMany('App\Candidate'); //לכל סטטוס יש הרבה מועמדים
    } 

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to'); //שאילתה של קוורי בילדר
        return self::find($nextstages)->all(); //סלף-שאילתה על הסטטוס - שולף את כל האיי די של הסטטוסים הבאים
    }
//הפונקציה כללית ולכן סטטית
    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get(); //משיכת הנתונים מטבלת נקסט סטיג'ס ונבנה שאילתה שתבדוק האם המעבר חוקי
        if(isset($allowed)) return true; //אם קיבלנו משהו (סטטוס לעבור אליו) אז טרו
        return false;
    }

}
