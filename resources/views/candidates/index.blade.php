@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if(Session::has('notallowed')) <!--במקרה והגייט מחזיר פאלס-->
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}} <!--הודעה אדומה-->
</div>
@endif
<!--הקישור זה היו-ר-ל של עמוד יצירת המשתמש-->
<div><a href =  "{{url('/candidates/create')}}"> Add new candidate</a></div>
<h1>List of candidates</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($candidates as $candidate) <!--המשתנה קנדידייט מוכר כיוון שנשלח מהקונטרולר-->
        <tr>
            <td>{{$candidate->id}}</td> <!--סוגריים כפולים מסולסלים מדפיסים את המשתנה כמו אקו-->
            <td>{{$candidate->name}}</td>
            <td>{{$candidate->email}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->user_id)) <!--בדיקה האם הוגדר למועמד יוזר אחראי-->
                          {{$candidate->owner->name}}  <!--החזרת שם היוזר-->
                        @else
                          Assign owner
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user) <!--הלולאה רצה על היוזרים-->
                      <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td>
            <td>
                <div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id)) <!--בדיקה האם הסטטוס הוא לא סופי ויש לו אופציות מעבר-->   
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id)) <!--בדיקה האם הוגדר למועמד סטטוס-->
                           {{$candidate->status->name}}
                        @else
                            Define status<!--במקרה שלא הוגדר למועמד סטטוס-->
                        @endif
                    </button>
                    @else 
                    {{$candidate->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null ) <!-- שימוש בנתיב המלא של הפונקציה שנפעיל על קנדידייט והיא תחזיר את הסטטוסים המותרים (בודקים האם לא ריק-->
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                         <a class="dropdown-item" href="{{route('candidates.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                </div>                            
            </td>                             
            <td>{{$candidate->created_at}}</td>
            <td>{{$candidate->updated_at}}</td>
            <td>
                <a href = "{{route('candidates.edit',$candidate->id)}}">Edit</a> <!--קישור ללחיצת על אדיט-->
            </td> 
            <td>
                    <a href = "{{route('candidate.delete',$candidate->id)}}">Delete</a><!--קישור ללחיצת על דלית-->
            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

